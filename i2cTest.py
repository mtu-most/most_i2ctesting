import Adafruit_GPIO.I2C as I2C

def I2cByteWrite(i2cAddr, i2cBus, i2cReg, dataByte):
        dev = I2C.get_i2c_device(i2cAddr,i2cBus)
        dev.writeList(int(i2cReg), dataByte)
        return

def I2cByteRead(i2cAddr, i2cBus, i2cReg):
    dev = I2C.get_i2c_device(i2cAddr,i2cBus)
    byte = dev.readRaw8()
    return byte

def TestMUX(i2cAddr, i2cBus, muxChannel):
    I2cByteWrite(i2cAddr, i2cBus, 0x00, muxChannel)
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
    if check is int(muxChannel,2):
        print("The MUX correctly switched to <" + muxChannel + ">")
    else:
        print("Ope! Not working! You got <" + str(check) + ">")
    return

#I2cByteWrite(0x56, 2, 0x00, 257)

dev = I2C.get_i2c_device(0x56,2)
hi = [66, 101, 116, 104, 97, 110, 121]

for i in range(7):
    dev.write8(0x00, hi[i])

