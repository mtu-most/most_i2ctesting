#!/usr/bin/python3

from serial import Serial
import time

arduino = Serial('/dev/ttyUSB0', 9600)
arduino.timeout = 1

while True:
    i = str(input("Press `ENTER` to read pH value from arduino, `q` to quit: ")).strip()
    if i == 'q' or i == 'quit' or i == 'Q' or i == 'Quit':
        print('Program stopped.')
        break
    arduino.write(str(5).strip().encode())
    time.sleep(1)
    print(arduino.readline().decode('ascii'))

arduino.close()
