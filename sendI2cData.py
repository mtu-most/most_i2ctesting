#!/usr/bin/python3

import Adafruit_GPIO.I2C as I2C
import argparse

MIN_VAL = 00.00
MAX_VAL = 14.00

def range_limited_float_type(arg):
    """ Type function for argparse - a float within some predefined bounds """
    try:
        f = float(arg)
    except ValueError:    
        raise argparse.ArgumentTypeError("Must be a floating point number")
    if f < MIN_VAL or f > MAX_VAL:
        raise argparse.ArgumentTypeError("Argument must be > " + str(MIN_VAL) + " and < " + str(MAX_VAL))
    return f

addr = 0x02
bus = 2
dev = I2C.get_i2c_device(addr,bus)

parser = argparse.ArgumentParser(description="Send a float to the ardunio.")
parser.add_argument('floatNum', help='floating point number', type=range_limited_float_type)
args = parser.parse_args()

floatStr = str(args.floatNum)
strLen = len(floatStr)

#dev.write8(0x00, strLen)
#print(strLen)

#for i in range(strLen):
#    dev.write8(0x00, ord(floatStr[i]))
#    print(ord(floatStr[i]))

asciiList = [1,1,1,1]

for i in range(strLen):
    asciiList[i] = ord(floatStr[i])

dev.writeList(0x00, asciiList)
