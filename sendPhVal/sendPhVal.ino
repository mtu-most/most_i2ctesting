String incomingByte;
int floatChars;
float phValue = 10.00;

void setup() {
  
  // Begin Serial communication with the BBB.
  Serial.begin(9600);
  // Seed `random()` with the floating voltage on pin A0.
  randomSeed(analogRead(A0));
  
}

void loop() {

  // Get random pH value (TODO: Replace this with actual pH read function.)
  phValue = (random(1, 101)*14.00)/100;

  // If there is nothing wrong with Serial.
  if (Serial.available() > 0) {

    // Wait for request from BBB.
    incomingByte = Serial.readStringUntil('\n');
    // Convert BBB's request byte from a String to an int... This line doesn't affect any functionality.
    floatChars = incomingByte.toInt();
    // Print the value of pH to the serial comm.
    Serial.print(phValue);
    
  }
  
}
