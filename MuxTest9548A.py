#!/usr/bin/python3

""" Testing the different multiplexer boards that we have... """

# Board 1: (TODO: Check for slow switching... DONE)
#   0,1,2,5,6,7 (only 0,1,6 worked in our first test on 2021/06/03)
''' Channel 3
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 17, in TestMUX
    I2cByteWrite(i2cAddr, i2cBus, 0x00, muxChannel)
  File "./MuxTest9548A.py", line 8, in I2cByteWrite
    dev.write8(int(i2cReg), int(dataByte,2))
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 114, in write8
    self._bus.write_byte_data(self._address, register, value)
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 322, in write_byte_data
    self._device.write(data)
TimeoutError: [Errno 110] Connection timed out
'''

''' Channel 4
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 51, in <module>
    TestMUX(addr, bus, channel)
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
TimeoutError: [Errno 110] Connection timed out
'''

# Board 2:
#   0,1,2,3,4,6,7 (0 doesn't work with app.py even though my switching program works fine.)
''' Channel 5
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
OSError: [Errno 121] Remote I/O error
'''

# Board 3:
#   1,2,4,6
''' Channel 0
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
TimeoutError: [Errno 110] Connection timed out
'''

''' Channel 3
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 17, in TestMUX
    I2cByteWrite(i2cAddr, i2cBus, 0x00, muxChannel)
  File "./MuxTest9548A.py", line 8, in I2cByteWrite
    dev.write8(int(i2cReg), int(dataByte,2))
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 114, in write8
    self._bus.write_byte_data(self._address, register, value)
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 322, in write_byte_data
    self._device.write(data)
TimeoutError: [Errno 110] Connection timed out
'''

''' Channel 5
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
TimeoutError: [Errno 110] Connection timed out
'''

# Channel 6 takes forever to switch, maybe not?? Nevermind it is fine now.

''' Channel 7
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
TimeoutError: [Errno 110] Connection timed out
'''

# Board 4: When this one fails, simply removing ground to power cycle the multiplexer board does not cause it to start working again, oops it actually does work.
#   1,2,4,5,6,7
''' Channel 0
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
TimeoutError: [Errno 110] Connection timed out
'''

''' Channel 3
Traceback (most recent call last):
  File "./MuxTest9548A.py", line 43, in <module>
    TestMUX(addr, bus, muxChannels[args.single])
  File "./MuxTest9548A.py", line 18, in TestMUX
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
  File "./MuxTest9548A.py", line 13, in I2cByteRead
    byte = dev.readRaw8()
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_GPIO/I2C.py", line 141, in readRaw8
    result = self._bus.read_byte(self._address) & 0xFF
  File "/usr/local/lib/python3.7/dist-packages/Adafruit_PureIO/smbus.py", line 171, in read_byte
    return ord(self._device.read(1))
TimeoutError: [Errno 110] Connection timed out
'''

import Adafruit_GPIO.I2C as I2C
import argparse

def I2cByteWrite(i2cAddr, i2cBus, i2cReg, dataByte):
        dev = I2C.get_i2c_device(i2cAddr,i2cBus)
        dev.write8(int(i2cReg), int(dataByte,2))
        return

def I2cByteRead(i2cAddr, i2cBus, i2cReg):
    dev = I2C.get_i2c_device(i2cAddr,i2cBus)
    byte = dev.readRaw8()
    return byte

def TestMUX(i2cAddr, i2cBus, muxChannel):
    I2cByteWrite(i2cAddr, i2cBus, 0x00, muxChannel)
    check = I2cByteRead(i2cAddr, i2cBus, 0x00)
    if check is int(muxChannel,2):
        print("The MUX correctly switched!")
    else:
        print("Ope! Not working! You got `" + str(check) + "`")
    return


parser = argparse.ArgumentParser(description="Test channels of the 9548A Mux.")
group = parser.add_mutually_exclusive_group()
group.add_argument('-s', '--single', dest='single', help='specify single channel to test', type=int, choices=range(0, 8), action='store')
group.add_argument('-i', '--ignore', dest='ignored', help='specify mux channels to be ingnored in test', type=int, nargs='+', choices=range(0, 8))
args = parser.parse_args()

muxChannels = ['00000001','00000010','00000100','00001000','00010000','00100000','01000000','10000000']
addr = 0x74
bus = 2

if (args.single is None) and (args.ignored is None):
    for channel in muxChannels:
        print("Testing channel... " + "`" + str(channel) + "`")
        TestMUX(addr, bus, channel)

elif args.single is not None:
    print("Testing channel... " + "`" + str(muxChannels[args.single]) + "`")
    TestMUX(addr, bus, muxChannels[args.single])

else:
    for i in sorted(args.ignored, reverse=True):
        del muxChannels[i]

    for channel in muxChannels:
        print("Testing channel... " + "`" + str(channel) + "`")
        TestMUX(addr, bus, channel)
